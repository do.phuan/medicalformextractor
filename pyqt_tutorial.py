import sys
import os
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
import MedicExtractor
import cv2

def selectFile():
    fileToDetectPath.setText(QFileDialog.getOpenFileName()[0])
    lowerGroupBox.setEnabled(True)
    processBtn.setEnabled(True)

def extractText():
    if ((fileToDetectPath.text()).endswith(".pdf")):
        gray = MedicExtractor.convertPdf2Jpegs(fileToDetectPath.text())
        text = MedicExtractor.extractTextFromPdf(gray, features)
        resultBlock.setText(text)
    # if ((fileToDetectPath.text()).endswith(".jpg")):
    #     gray = cv2.imread(fileToDetectPath.text(), cv2.IMREAD_GRAYSCALE)
    #     text = MedicExtractor.extractTextFromPdf(gray, features)
    #     resultBlock.setText(text)
#-----------------------------------------------------------------------------------------------------------------
try:
    MedicExtractor.extractLabels(os.getcwd() + "/sample/images/", os.getcwd() + "/sample/features/")
    features = MedicExtractor.loadLabels(os.getcwd() + "/sample/features/")
except FileNotFoundError:
    print("Failed to create samples features. Please contact development team for help!")
#-----------------------------------------------------------------------------------------------------------------
app = QApplication([])
panel = QDialog()
panel.setLayout(QGridLayout())


##### upper group box
upperGroupBox = QGroupBox('Main')
upperGroupBox.setMinimumSize(500, 100)
upperGroupBox.setLayout(QVBoxLayout())
panel.layout().addWidget(upperGroupBox, 1, 0)
# panel.layout().addStretch()

row_1 = QHBoxLayout()

fileToDetectPath = QLineEdit()
fileToDetectPath.move(10,30)
fileToDetectPath.setMinimumWidth(250)
browseBtn = QPushButton('Browse')
browseBtn.setDefault(True)
browseBtn.clicked.connect(selectFile)

processBtn = QPushButton('Extract')
processBtn.setDisabled(True)
processBtn.clicked.connect(extractText)

row_1.addWidget(fileToDetectPath)
row_1.addWidget(browseBtn)
row_1.addWidget(processBtn)

comboBox = QComboBox()
comboBox.addItem("CSV")
comboBox.addItem("XML")
comboBox.addItem("PLAIN TEXT")

row_2 = QHBoxLayout()
row_2.addWidget(QLabel('Output form:'))
row_2.addWidget(comboBox)

upperGroupBox.layout().addLayout(row_1)
upperGroupBox.layout().addLayout(row_2)

###### bottom left box
bottomLeftGroupBox = QGroupBox('Status')
bottomLeftGroupBox.setLayout(QVBoxLayout())
bottomLeftGroupBox.setMinimumSize(500, 150)

row = QHBoxLayout()
statusBlock = QTextEdit()
row.addWidget(statusBlock)
bottomLeftGroupBox.layout().addLayout(row)
panel.layout().addWidget(bottomLeftGroupBox, 2, 0)

###### lower group box
lowerGroupBox = QGroupBox('Result')
lowerGroupBox.setLayout(QVBoxLayout())
panel.layout().addWidget(lowerGroupBox, 1, 1, 2, 1)
# panel.layout().addStretch()

row = QHBoxLayout()
resultBlock = QTextEdit()
resultBlock.setMinimumSize(400, 800)
row.addWidget(resultBlock)
lowerGroupBox.layout().addLayout(row)
lowerGroupBox.setDisabled(True)



# panel.setGeometry(100, 100, 500, 350)
panel.setWindowTitle("Medical Form Extractor")
panel.show()
app.exec_()
