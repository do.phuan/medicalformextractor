# MEDICAL FORM RECOGNITION SYSTEM - LOCAL VERSION

An OCR application which was designed for entering data for clinical labs in Australia. 

## Getting started

Install all required applications that support application running.

### Prerequisites

**Python 2.7.x** or **Python 3.x:** Download and install from the link 
> https://www.python.org/downloads/

**Pip**

**Virtual environment** 

and others required application listed in **Requirements.txt**

### Running
After finishing the required installation, generate a virtual environment folder and install packages listed in *Requirements.txt* in it. Then change directory to virtualenv directory by typing:
```
cd <path_to_virtualenv_dir>
```

then activate the environment
```
source env/bin/activate
```

Now you are able to execute the app with its user interface by running:

```
python pyqt_tutorial.py
```

## Authors

* **An Do Phu** - *Initial work*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
