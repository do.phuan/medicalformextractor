#############################################################################
##
## Copyright (C) 2018 An Do
## Copyright (C) 2019 AS-Team
## All rights reserved.
##
## This is the main source code of the OCR system
## using in medical form extractor application
##
## "Redistribution and use in source and binary forms, with or without
## modification, are permitted provided that the following conditions are
## met:
##   * Redistributions of source code must retain the above copyright
##     notice, this list of conditions and the following disclaimer.
##   * Redistributions in binary form must reproduce the above copyright
##     notice, this list of conditions and the following disclaimer in
##     the documentation and/or other materials provided with the
##     distribution.
##   * Neither the name of AS-Team and its Subsidiary(-ies) nor
##     the names of its contributors may be used to endorse or promote
##     products derived from this software without specific prior written
##     permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
## LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
## DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
## THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
## (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
## OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
##
#############################################################################
import argparse
import cv2
import json
import math
import pickle
import shutil
import numpy as np
import OCRopus
import os
from  xml.etree import ElementTree as ET
from PIL import Image
from scipy import ndimage
from codecs import open
from pdf2image import convert_from_path, convert_from_bytes


MIN_MATCH_COUNT = 50

anchors = []
layouts = []
titles = []

with open(os.getcwd() + "/json/titles.json") as t:
    titles = json.load(t)
    
with open(os.getcwd() + "/json/layout.json") as l:
    layouts = json.load(l)

with open(os.getcwd() + "/json/anchor.json") as a:
    anchors = json.load(a)

class Label:
    def __init__(self, filesname, descriptor):
        self.filesname = filesname
        self.descriptor = descriptor

def preprocessing(path):
    img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    return img

def convertPdf2Jpegs(inputPath):
    pages = []
    temp = convert_from_path(inputPath)

    for p in temp:
        pages.append(np.array(p.convert('L')))

    return pages

def shiftImg(img, anchors):
    rows,cols = img.shape
    image = cropImage(img, 0, 300, 0, 400)

    cv2.imwrite("temp.jpg", image)
    p1, p2 = OCRopus.position("temp.jpg")
    os.remove("temp.jpg")

    M = np.float32([[1,0, int(anchors[0]) - int(p1)],[0,1, int(anchors[1]) - int(p2)]])
    dst = cv2.warpAffine(img,M,(cols,rows))

    cv2.imwrite("/Users/ando/Documents/MedicalForm/pdf1/rotated_shift.jpg", dst)
    return dst

# ---------------------------------------------------------------------------------------------------
def pickleKeypoints(descriptor, path):
    with open(path, 'wb') as f:
        pickle.dump(descriptor, f)
    f.close()

def unpickleKeypoint(path):
    with open(path, 'rb') as f:
        des = pickle.load(f)
    return des
# ---------------------------------------------------------------------------------------------------
def extractKeyFeatures(img):
    sift = cv2.xfeatures2d.SIFT_create()
    kp, des = sift.detectAndCompute(img, None)
    return kp, des

def getMatches(des1, des2):
    FLANN_INDEX_KDTREE = 0
    good = []

    index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
    search_params = dict(checks=50)

    flann = cv2.FlannBasedMatcher(index_params, search_params)
    matches = flann.knnMatch(des1, des2, k=2)

    for m, n in matches:
        if m.distance < 0.7 * n.distance:
            good.append(m)
    return good
#-----------------------------------------------------------------------------------------------------
def extractLabels(path, featuresDirectory):
    for p in os.listdir(path):
        if not os.path.isfile(os.path.join(featuresDirectory, p.replace(".jpg",".pickle"))):
            if p.endswith(".jpg"):
                kp, des = extractKeyFeatures(preprocessing(os.path.join(path, p)))
                pickleKeypoints(des, os.path.join(featuresDirectory, p.replace(".jpg",".pickle")))

def loadLabels(path):
    features = []
    for file in os.listdir(path):
        if file.endswith(".pickle"):
            temp = unpickleKeypoint(os.path.join(path, file))
            temp = Label(file.replace(".pickle",""), temp)
            features.append(temp)
    return features

def findFormsLayout(gray, labels):
    max = 0
    formsLayout = ""
    gray = cropImage(gray, 50, int(gray.shape[0]/6), 20, int(gray.shape[1]/4))
    kp, des = extractKeyFeatures(gray)
    for l in labels:
        matches = getMatches(des, l.descriptor)
        nMatches = len(matches)
        if nMatches > max and nMatches > MIN_MATCH_COUNT:
            max = nMatches
            formsLayout = l.filesname
    return formsLayout

# ---------------------------------------------------------------------------------------------------
def rotateImage(image):
    imgBefore = image[150:500, 0:1300]

    edges = cv2.Canny(imgBefore, 100, 100, apertureSize=3)
    lines = cv2.HoughLinesP(edges, 1, math.pi / 180.0, 100, minLineLength=60, maxLineGap=5)

    angles = []

    for x1, y1, x2, y2 in lines[0]:
        angle = math.degrees(math.atan2(y2 - y1, x2 - x1))
        angles.append(angle)

    medianAngle = np.median(angles)
    imgRotated = ndimage.rotate(image, medianAngle)

    return imgRotated

def cropImage(image, y1, y2, x1, x2):
    cropImg = image[y1:y2, x1:x2]
    return cropImg

def createDirectory(path):
    if not os.path.isdir(path):
        try:
            os.mkdir(path)
        except OSError:
            print ("Creation of the directory %s failed" % path)

def export2CSV(texts):
    status = False
    for text in texts:
        text.replace(":", ",")
    return status

def export2XML(texts):
    myData = ""
    data = ET.Element('data')
    for text in texts:
        if "NPages" in text.split():
            form = ET.SubElement(data, 'form')
        else:
            t = text.split("|")
            field = ET.SubElement(form, 'field')
            field.set('name', t[0].strip())

            value = ET.SubElement(field, 'value')
            value.text = t[1].strip()

            location_x = ET.SubElement(field, 'location')
            location_x.set('name', 'x')
            location_x.text = int(t[2])

            location_y = ET.SubElement(field, 'location')
            location_y.set('name', 'y')
            location_y.text = int(t[3])
    myData = ET.tostring(data, encoding='utf8', method='xml').decode()
    if myData != "":
        return True
    return False

def exportPlainTexts(texts):
    for text in texts:
        text.split("|")
        text = text[0].strip() + ": " + text[1].strip()
    return True

def extractTextFromPdf(path, pages, features):
    processedData = []

    data = ET.Element('data')

    for i in pages:
        gray = rotateImage(i)

        formsLayout = findFormsLayout(gray, features)

        title = titles[formsLayout]
        layout = layouts[formsLayout]
        anchor = anchors[formsLayout]

        if (formsLayout == "pathology") or (formsLayout == "labrequestform"):
            gray = shiftImg(gray, anchor)

        form = ET.SubElement(data, 'form')

        for t in title:
            img = cropImage(gray, int(layout[t]["y"]), int(layout[t]["y"]) + int(layout[t]["h"]),
                            int(layout[t]["x"]), int(layout[t]["x"]) + int(layout[t]["w"]))
            img = OCRopus.prepare(img)

            convertedImage = Image.fromarray(img)
            text = OCRopus.img_2_str(convertedImage, str(layout[t]["option"]), layout[t]["regex"])
            data = title + " | " + text + " | " + layout[t]["x"] + " | " + layout[t]["y"]
            processedData.append(data)
    return processedData
# ---------------------------------------------------------------------------------------------------
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('detect_file', help='The image for text detection.')
    args = parser.parse_args()

    try:
        extractLabels(os.getcwd() + "/samples/images/", os.getcwd() + "/samples/features/")
        features = loadLabels(os.getcwd() + "/samples/features/")
    except FileNotFoundError:
        print("Failed to create samples features. Please contact development team for help!")

    #------------------------------------------ Pdf file -----------------------------------------------------------------------------------------------
    if args.detect_file.endswith(".pdf"):
        pages = convertPdf2Jpegs(args.detect_file)
        extractTextFromPdf(args.detect_file, pages, features)
    else:
        print("Failed to open " + str(args.detect_file))