import cv2
from PIL import Image
from pytesseract import pytesseract as pt
import argparse
import os
import re
import numpy as np
import math

def dilate(image, kernelSize, iterations):
    kernel = np.ones((kernelSize,kernelSize),np.uint8)
    dilation = cv2.dilate(image,kernel,iterations = 1)
    return dilation

def erode(image, kernelSize, iterations):
    kernel = np.ones((kernelSize,kernelSize),np.uint8)
    erode = cv2.erode(image,kernel,iterations = iterations)
    return erode

def thresh(gray):
    ret, threshed = cv2.threshold(gray, 150,255,cv2.THRESH_BINARY)
    return ret, threshed

def blur(gray, kSize):
    blurred = cv2.medianBlur(gray, kSize)
    return blurred
def opening(gray, kSize):
    kernel = np.ones((kSize,kSize),np.uint8)
    return cv2.morphologyEx(gray, cv2.MORPH_OPEN, kernel)

def pre_processing(path, erode_kernel= 2, dilate_kernel = 2, iteration = 1):
    gray = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    gray = cv2.bitwise_not(gray)
    gray = blur(gray, 3)
    gray = dilate(erode(gray, erode_kernel, iteration), dilate_kernel, iteration)
    gray = blur(gray, 3)
    gray = opening(gray, 2)
    ret, thre = thresh(gray)
    return thre

def img_2_str(path, option, regex):
    text = pt.image_to_string(path, lang='eng', config=option )
    text = re.sub(r'\n\s*\n',' ',text,re.MULTILINE)
    text = re.sub('[^a-zA-Z0-9 \n\.\,\:\!\-\/\)\(\'\?]', '', text)
    text = re.sub('^[ \t]+|[ \t]+$','',text)
    text = re.sub('\n', " ", text)
    if regex != "":
        original_text = str(re.findall(regex, text))
        s = original_text.split("\'")
        if len(s) >= 2:
            text = s[1].replace("[(\'","").strip()
        else:
            text = ""
    return text

def findTextPosition(path):
    l = ""
    pt.run_tesseract(path, 'output', extension="box", lang='eng', config="hocr")
    for line in open("output.hocr"):
        if ("path/Metro" in line) or (">Metropath" in line):
            l = line
            break
    l = l[l.index("bbox "):l.index(";")]
    l = re.sub('[^0-9 ]','',l)
    l.strip()
    os.remove("output.hocr")
    return l.split()

def position(path):
    pos = findTextPosition(path)
    return pos[2], pos[3]

def prepare(gray):
    gray = cv2.cv2.resize(gray, None, fx=2, fy=2, interpolation=cv2.INTER_LINEAR)
    gray = blur(gray, 3)
    # ret, gray = cv2.threshold(gray, 200,255,cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    gray = cv2.bitwise_not(gray)
    gray = erode(gray, 1, 1)
    gray = dilate(gray, 1, 1)
    # gray = opening(gray, 1)
    ret, gray = cv2.threshold(gray, 100,255,cv2.THRESH_BINARY)
    gray = cv2.bitwise_not(gray)
    return gray